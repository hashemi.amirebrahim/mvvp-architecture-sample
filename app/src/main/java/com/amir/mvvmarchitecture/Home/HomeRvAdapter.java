package com.amir.mvvmarchitecture.Home;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.amir.mvvmarchitecture.Model.Product;
import com.amir.mvvmarchitecture.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeRvAdapter extends RecyclerView.Adapter<HomeRvAdapter.HomeViewHolder> {

    private List<Product> products;
    private OnProductItemClick onProductItemClick;

    public HomeRvAdapter(List<Product> products, OnProductItemClick onProductItemClick) {
        this.products = products;
        this.onProductItemClick = onProductItemClick;
    }

    @NonNull
    @Override
    public HomeViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_item2, parent, false);
        return new HomeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull HomeViewHolder holder, int position) {
        Product product = products.get(position);
        Picasso.get().load(product.getPic()).into(holder.imgProductItem);
        holder.tvProductItemTitle.setText(product.getTitle());
        holder.tvProductItemPPrice.setText("قیمت قبلی: " + product.getpPrice());
        holder.tvProductItemPrice.setText("قیمت: " + product.getPrice());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onProductItemClick.onItemClick(product.getId(), holder.getAdapterPosition());
            }
        });

        findLastItem(product, holder);
    }

    private void findLastItem(Product product, HomeViewHolder holder) {
        if (product == products.get(products.size() - 1)) {
            holder.itemBottomLine.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class HomeViewHolder extends RecyclerView.ViewHolder {

        ImageView imgProductItem;
        TextView tvProductItemTitle, tvProductItemPPrice, tvProductItemPrice;
        View itemBottomLine;
        public HomeViewHolder(@NonNull View itemView) {
            super(itemView);
            imgProductItem = itemView.findViewById(R.id.imgProductItem);
            tvProductItemTitle = itemView.findViewById(R.id.tvProductItemTitle);
            tvProductItemPPrice = itemView.findViewById(R.id.tvProductItemPPrice);
            tvProductItemPrice = itemView.findViewById(R.id.tvProductItemPrice);
            itemBottomLine = itemView.findViewById(R.id.itemBottomLine);
        }
    }

    public interface OnProductItemClick {
        void onItemClick(String id, int productPosition);
    }
}
