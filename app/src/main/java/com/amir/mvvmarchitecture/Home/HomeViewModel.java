package com.amir.mvvmarchitecture.Home;

import android.util.Log;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.amir.mvvmarchitecture.Model.ApiService;
import com.amir.mvvmarchitecture.Model.MutableLiveDataError;
import com.amir.mvvmarchitecture.Model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeViewModel extends ViewModel {

    private static final String TAG = "MY_LOG";

    private ApiService apiService;

    public MutableLiveData<List<Product>> productsMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<MutableLiveDataError> errorMutableLiveData = new MutableLiveData<>();

    public HomeViewModel(ApiService apiService) {
        this.apiService = apiService;
        getProductsFromServer();
    }

    public void getProductsFromServer() {
        apiService.getProductList().enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                productsMutableLiveData.setValue(response.body());
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {

                MutableLiveDataError error = new MutableLiveDataError();
                error.setStatus("Error");
                error.setMessage(t.toString());
                errorMutableLiveData.setValue(error);
                Log.i(TAG, "onFailure: " + t.toString());
            }
        });
    }

    //By Setup this function we set a limit to our view to can't set Value in livedata from our view.
    public LiveData<List<Product>> getProducts() {
        return productsMutableLiveData;
    }

    public LiveData<MutableLiveDataError> getError() {
        return errorMutableLiveData;
    }

}
