package com.amir.mvvmarchitecture.Home;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.amir.mvvmarchitecture.Model.ApiService;

public class HomeViewModelFactory implements ViewModelProvider.Factory {

    private ApiService apiService;

    public HomeViewModelFactory(ApiService apiService) {
        this.apiService = apiService;
    }

    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        return (T) new HomeViewModel(apiService);
    }

}
