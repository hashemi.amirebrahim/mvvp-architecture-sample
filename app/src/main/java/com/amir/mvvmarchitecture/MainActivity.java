package com.amir.mvvmarchitecture;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.amir.mvvmarchitecture.Home.HomeRvAdapter;
import com.amir.mvvmarchitecture.Home.HomeViewModel;
import com.amir.mvvmarchitecture.Home.HomeViewModelFactory;
import com.amir.mvvmarchitecture.Model.ApiClient;
import com.amir.mvvmarchitecture.Model.ApiService;
import com.amir.mvvmarchitecture.Model.MutableLiveDataError;
import com.amir.mvvmarchitecture.Model.Product;

import java.util.List;

public class MainActivity extends AppCompatActivity implements HomeRvAdapter.OnProductItemClick {

    private static final String TAG = "MY_LOG";
    RecyclerView rvMainProducts;
    HomeRvAdapter adapter;

    public List<Product> productList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUpRv();

        HomeViewModel homeViewModel = new ViewModelProvider(MainActivity.this,
                new HomeViewModelFactory(ApiClient.getClient().create(ApiService.class))).get(HomeViewModel.class);
        homeViewModel.getProducts().observe(MainActivity.this, new Observer<List<Product>>() {
            @Override
            public void onChanged(List<Product> products) {
                Log.i(TAG, "Products List Size: " + products.size());
                productList = products;

                adapter = new HomeRvAdapter(products, MainActivity.this);
                rvMainProducts.setAdapter(adapter);
            }
        });

        homeViewModel.getError().observe(MainActivity.this, new Observer<MutableLiveDataError>() {
            @Override
            public void onChanged(MutableLiveDataError error) {
                Log.i(TAG, "Error happened: " + error.getMessage());
                Toast.makeText(MainActivity.this, "E: " + error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void setUpRv() {
        rvMainProducts = findViewById(R.id.rvMainProducts);
        rvMainProducts.setLayoutManager(new LinearLayoutManager(MainActivity.this));
    }

    @Override
    public void onItemClick(String id, int productPosition) {
        Toast.makeText(this, "" + productList.get(productPosition).getTitle(), Toast.LENGTH_SHORT).show();
    }
}