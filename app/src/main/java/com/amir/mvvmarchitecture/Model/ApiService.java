package com.amir.mvvmarchitecture.Model;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiService {

    @GET("readamazing.php")
    Call<List<Product>> getProductList();
}
