package com.amir.mvvmarchitecture.Model;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    //private static final String BASE_URL = "http://192.168.43.71/android/digi_api/digikala/";
    private static final String BASE_URL = "http://172.19.0.1/android/digi_api/digikala/";
    //private static final String BASE_URL = "http://127.0.0.1/android/digi_api/digikala/";
    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }
}
